/*
   Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
#ifndef XAODMUONPREPDATA_RpcStrip2DFWD_H
#define XAODMUONPREPDATA_RpcStrip2DFWD_H
#include "xAODMuonPrepData/RpcMeasurementFwd.h"
/** @brief Forward declaration of the xAOD::RpcStrip2D */
namespace xAOD{
   class RpcStrip2D_v1;
   using RpcStrip2D = RpcStrip2D_v1;

   class RpcStrip2DAuxContainer_v1;
   using RpcStrip2DAuxContainer = RpcStrip2DAuxContainer_v1;
}
#endif
