/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

/// @author Tadej Novak



#include <TriggerAnalysisAlgorithms/TrigEventSelectionAlg.h>
#include <TriggerAnalysisAlgorithms/TrigPrescalesAlg.h>
#include <TriggerAnalysisAlgorithms/TrigGlobalEfficiencyAlg.h>
#include <TriggerAnalysisAlgorithms/TrigMatchingAlg.h>


DECLARE_COMPONENT (CP::TrigEventSelectionAlg)
DECLARE_COMPONENT (CP::TrigPrescalesAlg)
DECLARE_COMPONENT (CP::TrigGlobalEfficiencyAlg)
DECLARE_COMPONENT (CP::TrigMatchingAlg)

